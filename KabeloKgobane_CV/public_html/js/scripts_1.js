﻿jQuery.noConflict()(function($){
	$(document).ready(function() {
		
	$("li.works").click(function () {
      elementClick = $(this).attr("href");
      destination = $('a.works').offset().top;
      if($.browser.safari){
        $('body').animate( { scrollTop: destination }, 1100, 'linear' );
      }else{
        $('html').animate( { scrollTop: destination }, 1100, 'linear' );
      }
      return false;
    });

	$("li.profile").click(function () {
      elementClick = $(this).attr("href");
      destination = $('a.profile').offset().top;
      if($.browser.safari){
        $('body').animate( { scrollTop: destination }, 1100, 'linear' );
      }else{
        $('html').animate( { scrollTop: destination }, 1100, 'linear' );
      }
      return false;
    });

	$("li.contact").click(function () {
      elementClick = $(this).attr("href");
      destination = $('a.contact').offset().top;
      if($.browser.safari){
        $('body').animate( { scrollTop: destination }, 1100, 'linear' );
      }else{
        $('html').animate( { scrollTop: destination }, 1100, 'linear' );
      }
      return false;
    });


/***************************************************
					Scroll in Portfolio
***************************************************/

	$('.scroll-pane').jScrollPane(
	{
		showArrows: true,
		verticalArrowPositions: 'os',			
	}
	);




	});
});


/***************************************************
					Twitter
***************************************************/

jQuery.noConflict()(function($){
$(document).ready(function() {
	
	  $(".tweet").tweet({
        	count: 4,
        	username: "zesky",
        	loading_text: "loading twitter..."      
		});

});
});


/***************************************************
					Item ColorBox
***************************************************/
jQuery.noConflict()(function($){
$(document).ready(function() {
	$('a[data-rel="zoom-img"]').colorbox({rel:'portfolio'});

	
	$('.to-top').stickyfloat({ duration: 750 });

$('.to-top').click(function(e){
	e.preventDefault();
	$('html, body').animate({scrollTop:0}, 1750);
});	
});
});


/***************************************************
					Inputs
***************************************************/

jQuery.noConflict()(function($){
	$(document).ready(function() {
		if(!Modernizr.input.placeholder){
			$("input").each(
			function(){
			if($(this).val()=="" && $(this).attr("placeholder")!=""){
			$(this).val($(this).attr("placeholder"));
			$(this).focus(function(){
			if($(this).val()==$(this).attr("placeholder")) $(this).val("");
			});
			$(this).blur(function(){
			if($(this).val()=="") $(this).val($(this).attr("placeholder"));
			});
			}
		});
		
		$("textarea").each(
			function(){
			if($(this).val()=="" && $(this).attr("placeholder")!=""){
			$(this).val($(this).attr("placeholder"));
			$(this).focus(function(){
			if($(this).val()==$(this).attr("placeholder")) $(this).val("");
			});
			$(this).blur(function(){
			if($(this).val()=="") $(this).val($(this).attr("placeholder"));
			});
			}
		});
		
		}
	});
});
